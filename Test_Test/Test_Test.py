import json
import cv2 as cv
import numpy as np
import sys
import glob
import os

#CONSTANTS
COLORS_VALUES = {
    'blue' : {
        'min_hue' : 106,
        'max_hue' : 126,
        'min_sat' : 100,
        'max_sat' : 255,
        'min_val' : 100,
        'max_val' : 255
    },
    'red': {
        'min_hue': 0,
        'max_hue': 12,
        'min_sat': 40,
        'max_sat': 255,
        'min_val': 80,
        'max_val': 255
    },
    'yellow': {
        'min_hue': 22,
        'max_hue': 42,
        'min_sat': 60,
        'max_sat': 255,
        'min_val': 40,
        'max_val': 255
    },
    'grey': {
        'min_hue': 73,
        'max_hue': 102,
        'min_sat': 0,
        'max_sat': 60,
        'min_val': 0,
        'max_val': 176
    },
    'white': {
        'min_hue': 35,
        'max_hue': 68,
        'min_sat': 0,
        'max_sat': 25,
        'min_val': 181,
        'max_val': 255
    },
}

BLUE_MIN = (106, 100, 100)
BLUE_MAX = (126, 255, 255)

RED_MIN = (0, 40, 80)
RED_MAX = (12, 255, 255)

YELLOW_MIN = (22, 60, 40)
YELLOW_MAX = (42, 255, 255)

GRAY_MIN= (73, 0, 0)
GRAY_MAX = (102, 60, 176)

WHITE_MIN = (35, 0, 181)
WHITE_MAX = (68, 25, 255)

BACK_COLOR_MIN = (0, 0, 141)
BACK_COLOR_MAX = (180, 47, 255)

ZOOM_VALUE = 100
RESIZE_VALUE = 1.2

def get_sys_args():
    imgs_path, init_json, result_json = sys.argv[1:]
    return imgs_path, init_json, result_json


def load_init_json(json_path):
    try:
        with open(json_path) as file:
            json_file = json.load(file)
    except():
        print("Nie udało się załadować wejściowego pliku JSON")
        sys.exit(0)
    return json_file

def load_images(imgs_path):
    imgs_names = []
    try:
        for image in glob.glob(imgs_path+'\\img_*.jpg'):
            img = cv.imread(image)
            img = cv.resize(img, None, fx=0.5, fy=0.5)
            name = os.path.basename(image)
            imgs_names.append((img, name))
    except():
        print("Nie udało się załadować zdjęć")
        sys.exit(0)
    return imgs_names


def zoom_img(img, zoom_val):
    if len(img.shape) == 2:
        row, col = img.shape[0:2]
        zooming = [zoom_val*2+row, zoom_val*2+col]
        zoomed_img = np.full(zooming, 255, dtype=np.uint8)
        zoomed_img[zoom_val:zoom_val+row, zoom_val:zoom_val+col] = img
        return zoomed_img
    else:
        row, col = img.shape[0:2]
        zooming = [row, col, 3]
        zoomed_img = np.zeros(zooming, dtype=np.uint8)
        zoomed_img[0:row, 0:col, :3] = img
        return zoomed_img

def img_cleaning(thresh):
    kernel = np.ones((3, 3), np.uint8)
    dilate = cv.dilate(thresh, kernel, iterations=3)
    opening = cv.morphologyEx(dilate, cv.MORPH_OPEN, kernel, iterations=2)
    erode = cv.erode(opening, kernel, iterations=16)
    closing = cv.morphologyEx(erode, cv.MORPH_CLOSE, kernel, iterations=0)
    # closing = cv.resize(closing, None, fx=0.5, fy=0.5)
    # cv.imshow('clean_img', closing)
    # cv.waitKey(0)
    return closing


def block_cleaning(block):
    kernel = np.ones((3, 3), np.uint8)
    erode = cv.erode(block, kernel, iterations=2)
    kernel = np.ones((5, 5), np.uint8)
    dilate = cv.dilate(erode, kernel, iterations=5)
    return dilate

def color_filtering(block, color_value):
    raw_block = cv.inRange(block, (color_value['min_hue'], color_value['min_sat'], color_value['min_val']),
                                 (color_value['max_hue'], color_value['max_sat'], color_value['max_val']))
    clean_block = block_cleaning(raw_block)
    contours, hierarchy = cv.findContours(clean_block, cv.RETR_TREE, cv.CHAIN_APPROX_NONE)
    color_contures = []
    for contour in contours:
        hull = cv.convexHull(contour)
        area = cv.contourArea(hull)

        if area > 4000:
            color_contures.append(contour)
    return color_contures


def final_assignment(init_json, blocks, img_name):
    while len(init_json[img_name]) < len(blocks):
        id = 0
        min_result = 10000
        for i,block in enumerate(blocks):
            if min_result > block['result']:
                min_result = block['result']
                id = i
        blocks.pop(id)


def data_saving(init_json, exit_json, exit_json_path):
    init_json = data_preparing(init_json)
    init_json = data_comparing(exit_json, init_json)
    exit_json = final_data_preparing(init_json)
    with open(exit_json_path, 'w') as file:
        json.dump(exit_json, file)


def data_preparing(init_json):
    for name, list in init_json.items():
        for block in list:
            block['result'] = ''
    return init_json



def data_comparing(detection_json, init_json):
    for blocks_matched in range(5,-1,-1):
        for name, list in init_json.items():
            for detection_name, detected_list in detection_json.items():
                if detection_name == name:
                    for result in detected_list:
                        if result['result'] != '':
                            for block in list:
                                if block['result'] == '':
                                    if compare_blocks(block, result,blocks_matched):
                                        block['result'] = result['result']
                                        result['result'] = ''
    return init_json


def compare_blocks(block1, block2, blocks_number):
    items = {k: block1[k] for k in block1 if k in block2 and block1[k] == block2[k]}
    if len(items) == blocks_number:
        return True
    return False


def final_data_preparing(entry_json):
    output_dict = {}

    for img_name, img_list in entry_json.items():
        output_list = []
        for block in img_list:
            output_list.append(block['result'])
        output_dict[img_name] = output_list

    return output_dict

if len(sys.argv) == 1:
    imgs_path = 'E:\\STUDIA\\\AIR\\Sem2\\SISW\\Holes_detection_project\\public\\imgs'
    init_json_path = 'E:\\STUDIA\\AIR\\Sem2\\SISW\\Holes_detection_project\\public\\files\\public.json'
    result_json = 'E:\\STUDIA\\AIR\\Sem2\\SISW\\Holes_detection_project\\public\\files\\output.json'
elif len(sys.argv) == 4:
    imgs_path, init_json_path, result_json = get_sys_args()
else:
    print("Podano błędnę argumenty wejściowe")
    sys.exit(0)

images = load_images(imgs_path)
entry_json = load_init_json(init_json_path)

results = {}

for image, name in images:
    final_results = []
    name = os.path.splitext(name)[0]
    hsv = image.copy()
    hsv = cv.cvtColor(hsv, cv.COLOR_BGR2HSV)
    background_filtered = cv.inRange(hsv, BACK_COLOR_MIN, BACK_COLOR_MAX)
    white_threshold = cv.inRange(hsv, WHITE_MIN, WHITE_MAX)
    background_filtered = cv.bitwise_not(background_filtered)
    processed_img = cv.bitwise_or(white_threshold, background_filtered)
    processed_img = cv.bitwise_not(processed_img)
    processed_img = zoom_img(processed_img, ZOOM_VALUE)
    clean_img = img_cleaning(processed_img)
    # clean_img= cv.resize(clean_img, None, fx=0.5, fy=0.5)
    # cv.imshow("img_filtered", clean_img)
    # cv.waitKey()
    contours, hierarchy = cv.findContours(clean_img, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    blocks_contours = []
    for contour in contours:
        contour_area = cv.contourArea(contour)
        if contour_area > 15000:
            blocks_contours.append(contour)

    # contours_poly = [None] * len(blocks_contours)
    # boundRect = [None] * len(blocks_contours)

    block_cropped = []
    img_cropping = hsv.copy()
    img_cropping = zoom_img(img_cropping, ZOOM_VALUE)
    for contour in blocks_contours[1:]:
        rectangle = cv.minAreaRect(contour)
        box = cv.boxPoints(rectangle)
        box = np.intp(box)
        w = rectangle[1][0]
        h = rectangle[1][1]
        x_box = [i[0] for i in box]
        y_box = [i[1] for i in box]
        x1 = min(x_box)
        x2 = max(x_box)
        y1 = min(y_box)
        y2 = max(y_box)

        rotation_flag = False
        rot_angle = rectangle[2]
        if rot_angle < -45:
            rot_angle += 90
            rotation_flag = True

        box_center = (int((x1 + x2) / 2), int((y1 + y2) / 2))
        box_size = (int(RESIZE_VALUE * (x2 - x1)), int(RESIZE_VALUE * (y2 - y1)))

        M = cv.getRotationMatrix2D((box_size[0] / 2, box_size[1] / 2), rot_angle, 1.0)

        roi = cv.getRectSubPix(img_cropping, box_size, box_center)
        roi = cv.warpAffine(roi, M, box_size)

        roi_w = w if not rotation_flag else h
        roi_h = h if not rotation_flag else w

        proper_roi = cv.getRectSubPix(roi,
                                      (int(roi_w * RESIZE_VALUE), int(roi_h * RESIZE_VALUE)),
                                      (box_size[0] / 2, box_size[1] / 2))
        block_cropped.append(proper_roi)


    for i, block in enumerate(block_cropped):
        block_dict = {'red': '',
                      'blue': '',
                      'white': '',
                      'grey': '',
                      'yellow': '',
                      'result': ''}

        for k, color in COLORS_VALUES.items():
            detected_blocks = color_filtering(block, color)
            block_dict[k] = str(len(detected_blocks))

        block = cv.cvtColor(block, cv.COLOR_HSV2BGR)
        block = cv.cvtColor(block, cv.COLOR_BGR2GRAY)
        circles = cv.HoughCircles(block, cv.HOUGH_GRADIENT, 1, 31,
                                  param1=160, param2=11, minRadius=9, maxRadius=14)
        if circles is not None:
            circles = np.uint16(np.around(circles))
            block_dict["result"] = circles.shape[1]
        else:
            block_dict["result"] = 0
        final_results.append(block_dict)
    final_assignment(entry_json, final_results, name)
    results[name] = final_results

data_saving(entry_json, results, result_json)


