import cv2 as cv
import numpy as np
import json
import sys
import glob
import os

# CONSTANTS
BLUE_MIN = (90, 80, 80)
BLUE_MAX = (130, 255, 255)

RED_MIN = (0, 40, 40)
RED_MAX = (12, 255, 255)

YELLOW_MIN = (15, 100, 100)
YELLOW_MAX = (30, 255, 255)

GRAY_MIN = (40, 18, 0)
GRAY_MAX = (100, 50, 170)

WHITE_MIN = (35, 8, 180)
WHITE_MAX = (179, 40, 255)

ZOOM_VALUE = 100


def run_arguments():
    imgs_path, init_json, result_json = sys.argv[1:]
    return imgs_path, init_json, result_json


def load_init_json(json_path):
    try:
        with open(json_path) as file:
            json_file = json.load(file)
    except():
        print("Nie udało się załadować wejściowego pliku JSON")
        sys.exit(0)
    return json_file


def load_images(imgs_path):
    imgs_names = []
    try:
        for image in glob.glob(imgs_path+'\\img_*.jpg'):
            img = cv.imread(image)
            img = cv.resize(img, None, fx=0.5, fy=0.5)
            name = os.path.basename(image)
            imgs_names.append((img, name))
    except():
        print("Nie udało się załadować zdjęć")
        sys.exit(0)
    return imgs_names


def block_cleaning(block):
    kernel = np.ones((3, 3), np.uint8)
    erode = cv.erode(block, kernel, iterations=2)
    kernel = np.ones((5, 5), np.uint8)
    dilate = cv.dilate(erode, kernel, iterations=5)
    return dilate


def img_cleaning(thresh):
    kernel = np.ones((3, 3), np.uint8)
    dilate = cv.dilate(thresh, kernel, iterations=3)
    opening = cv.morphologyEx(dilate, cv.MORPH_OPEN, kernel, iterations=2)
    erode = cv.erode(opening, kernel, iterations=16)
    closing = cv.morphologyEx(erode, cv.MORPH_CLOSE, kernel, iterations=0)
    return closing


def zoom_img(img, zoom_val):
    if len(img.shape) == 2:
        row, col = img.shape[0:2]
        zooming = [zoom_val*2+row, zoom_val*2+col]
        zoomed_img = np.full(zooming, 255, dtype=np.uint8)
        zoomed_img[zoom_val:zoom_val+row, zoom_val:zoom_val+col] = img
        return zoomed_img
    else:
        row, col = img.shape[0:2]
        zooming = [row, col, 3]
        zoomed_img = np.zeros(zooming, dtype=np.uint8)
        zoomed_img[0:row, 0:col, :3] = img
        return zoomed_img


def k_means(img):
    results_dict = {'red': '',
                   'blue': '',
                   'white': '',
                   'grey': '',
                   'yellow': '',
                   'result': ''}
    Z = np.float32(img.reshape((-1, 3)))

    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    K = 5
    _, labels, centers = cv.kmeans(Z, K, None, criteria, 10, cv.KMEANS_RANDOM_CENTERS)
    labels = labels.reshape((img.shape[:-1]))
    reduced = np.uint8(centers)[labels]
    result = []
    filtered_blue = []
    filtered_red = []
    filtered_yellow = []
    filtered_white = []
    filtered_grey = []
    for i, c in enumerate(centers):
        mask = cv.inRange(labels, i, i)
        mask = np.dstack([mask] * 3)
        ex_img = cv.bitwise_and(img, mask)
        ex_reduced = cv.bitwise_and(reduced, mask)
        img_blue = ex_img.copy()
        img_red = ex_img.copy()
        img_yellow = ex_img.copy()
        img_grey = ex_img.copy()
        img_white = ex_img.copy()

        img_blue = cv.cvtColor(img_blue, cv.COLOR_BGR2HSV)
        img_red = cv.cvtColor(img_red, cv.COLOR_BGR2HSV)
        img_yellow = cv.cvtColor(img_yellow, cv.COLOR_BGR2HSV)
        img_white = cv.cvtColor(img_white, cv.COLOR_BGR2HSV)
        img_grey = cv.cvtColor(img_grey, cv.COLOR_BGR2HSV)

        img_blue = cv.inRange(img_blue, BLUE_MIN, BLUE_MAX)
        img_blue = block_cleaning(img_blue)
        contours_blue, hierarchy = cv.findContours(img_blue, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for contour in contours_blue:
            hull = cv.convexHull(contour)
            area = cv.contourArea(hull)

            if area > 1000:
                filtered_blue.append(contour)

        img_red = cv.inRange(img_red, RED_MIN, RED_MAX)
        img_red = block_cleaning(img_red)
        contours_red, hierarchy = cv.findContours(img_red, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for contour in contours_red:
            hull = cv.convexHull(contour)
            area = cv.contourArea(hull)

            if area > 1000:
                filtered_red.append(contour)

        img_yellow = cv.inRange(img_yellow, YELLOW_MIN, YELLOW_MAX)
        img_yellow = block_cleaning(img_yellow)
        contours_yellow, hierarchy = cv.findContours(img_yellow, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for contour in contours_yellow:
            hull = cv.convexHull(contour)
            area = cv.contourArea(hull)

            if area > 1000:
                filtered_yellow.append(contour)

        img_white = cv.inRange(img_white, WHITE_MIN, WHITE_MAX)
        img_white = block_cleaning(img_white)
        contours_white, hierarchy = cv.findContours(img_white, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for contour in contours_white:
            hull = cv.convexHull(contour)
            area = cv.contourArea(hull)

            if area > 1000:
                filtered_white.append(contour)

        img_grey = cv.inRange(img_grey, GRAY_MIN, GRAY_MAX)
        img_grey = block_cleaning(img_grey)
        contours_grey, hierarchy = cv.findContours(img_grey, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)

        for contour in contours_grey:
            hull = cv.convexHull(contour)
            area = cv.contourArea(hull)

            if area > 1000:
                filtered_grey.append(contour)

        result.append(ex_img)
        results_dict['blue'] = str(len(filtered_blue))
        results_dict['red'] = str(len(filtered_red))
        results_dict['yellow'] = str(len(filtered_yellow))
        results_dict['grey'] = str(len(filtered_grey))
        results_dict['white'] = str(len(filtered_white))
    img_gs = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    circles = cv.HoughCircles(img_gs, cv.HOUGH_GRADIENT, 1, 31,
                              param1=160, param2=11, minRadius=9, maxRadius=14)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        results_dict["result"] = circles.shape[1]
    else:
        results_dict["result"] = 0
    return result, results_dict


def final_assignment(init_json, blocks, img_name):
    while len(init_json[img_name]) < len(blocks):
        id = 0
        save_condition = 10000
        for i,block in enumerate(blocks):
            if save_condition > block['result']:
                save_condition = block['result']
                id = i
        blocks.pop(id)


def data_preparing(init_json):
    for name, list in init_json.items():
        for block in list:
            block['result'] = ''
    return init_json


def data_comparing(final_json, init_json):
    for blocks_matched in range(5,-1,-1):
        for name, list in init_json.items():
            for detection_name, detected_list in final_json.items():
                if detection_name == name:
                    for result in detected_list:
                        if result['result'] != '':
                            for block in list:
                                if block['result'] == '':
                                    if compare_blocks(block, result,blocks_matched):
                                        block['result'] = result['result']
                                        result['result'] = 19
    return init_json


def compare_blocks(block1, block2, blocks_number):
    items = {k: block1[k] for k in block1 if k in block2 and block1[k] == block2[k]}
    if len(items) == blocks_number:
        return True
    return False


def final_data_preparing(init_json):
    output_dict = {}
    for img_name, img_list in init_json.items():
        output_list = []
        for block in img_list:
            output_list.append(block['result'])
        output_dict[img_name] = output_list
    return output_dict



if len(sys.argv) == 1:
    imgs_path = 'E:\\STUDIA\\\AIR\\Sem2\\SISW\\Holes_detection_project\\public\\imgs'
    init_json_path = 'E:\\STUDIA\\AIR\\Sem2\\SISW\\Holes_detection_project\\public\\files\\public.json'
    result_json = 'E:\\STUDIA\\AIR\\Sem2\\SISW\\Holes_detection_project\\public\\files\\output.json'
elif len(sys.argv) == 4:
    imgs_path, init_json_path, result_json = run_arguments()
else:
    print("Podano błędnę argumenty wejściowe")
    sys.exit(0)

images = load_images(imgs_path)
initial_json = load_init_json(init_json_path)

results = {}

for image, name in images:
    img = image
    name = os.path.splitext(name)[0]
    final_results = []
    img_hsv = cv.cvtColor(img.copy(), cv.COLOR_BGR2HSV)
    background_filtered = cv.inRange(img_hsv, (0, 0, 141), (180, 47, 255))
    img_white = cv.inRange(img_hsv, (35, 0, 181), (68, 25, 255))
    background_filtered = cv.bitwise_not(background_filtered)
    processed_img = cv.bitwise_or(img_white, background_filtered)
    processed_img = cv.bitwise_not(processed_img)
    clean_img = img_cleaning(processed_img)

    contours, hierarchy = cv.findContours(clean_img, cv.RETR_TREE, cv.CHAIN_APPROX_SIMPLE)
    big_contours = []
    for contour in contours[1:]:
            area = cv.contourArea(contour)
            if area > 4000:
                big_contours.append(contour)

    contours_poly = [None]*len(big_contours)
    boundRect = [None]*len(big_contours)
    resize_coefficient = 1.2
    cropped_blocks = []

    for i, c in enumerate(big_contours):
            contours_poly[i] = cv.approxPolyDP(c, 3, True)
            boundRect[i] = cv.minAreaRect(contours_poly[i])
            color = (0, 0, 255)
            box = cv.boxPoints(boundRect[i])
            box = np.intp(box)
            w = boundRect[i][1][0]
            h = boundRect[i][1][1]
            x_box = [i[0] for i in box]
            y_box = [i[1] for i in box]
            x1 = min(x_box)
            x2 = max(x_box)
            y1 = min(y_box)
            y2 = max(y_box)

            rotation_flag = False
            rot_angle = boundRect[i][2]
            if rot_angle < -45:
                rot_angle += 90
                rotation_flag = True

            box_center = (int((x1 + x2) / 2), int((y1 + y2) / 2))
            box_size = (int(resize_coefficient * (x2 - x1)), int(resize_coefficient * (y2 - y1)))
            rotation_matrix = cv.getRotationMatrix2D((box_size[0] / 2, box_size[1] / 2), rot_angle, 1.0)
            roi = cv.getRectSubPix(img, box_size, box_center)
            roi = cv.warpAffine(roi, rotation_matrix, box_size)
            roi_w = w if not rotation_flag else h
            roi_h = h if not rotation_flag else w
            proper_roi = cv.getRectSubPix(roi, (int(roi_w * resize_coefficient), int(roi_h * resize_coefficient)),
                                          (box_size[0] / 2, box_size[1] / 2))
            proper_roi = zoom_img(proper_roi, ZOOM_VALUE)
            cropped_blocks.append(proper_roi)

    for block in cropped_blocks:
        colors_detected, result = k_means(block)
        final_results.append(result)
    final_assignment(initial_json, final_results, name)
    results[name] = final_results

initial_json = data_preparing(initial_json)
initial_json = data_comparing(results, initial_json)
exit_json = final_data_preparing(initial_json)
with open(result_json, 'w') as file:
    json.dump(exit_json, file)
